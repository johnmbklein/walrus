$(function() {

  $("#dog").click(function(){
    $("#woof").removeClass("hidden");
    $("ul#doglist").prepend("<li>Woof!");
  })

  $("#cat").click(function(){
    $("#meow").removeClass("hidden");
    $("ul#catlist").prepend("<li>Meow!");
  })

  $("#dog").mouseout(function(){
    $("#woof").addClass("hidden");
  })

  $("#cat").mouseout(function(){
    $("#meow").addClass("hidden");
  })


});
