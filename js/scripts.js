$(function() {

  $("h1").click(function() {
    alert("This is a header.");
  });

  $("p").click(function() {
    $(this).addClass("highlighted");
  });

  // $("img").click(function() {
  //   alert("This is an image.");
  // });

  $("h2").dblclick(function() {
    prompt("why did you click on this?");
  });

  // $("img").click(function() {
  //   $("img").fadeIn("slow");
  // });

  $(".clickable").click(function() {
    $("#initial-hidden").fadeToggle();
    $("#initial-shown").fadeToggle();
  });

  $("button#yellow").click(function() {
    $("body").removeClass();
    $("body").addClass("yellow-background");
  });

  $("button#red").click(function() {
    $("body").removeClass();
    $("body").addClass("red-background");
  });

  $("button#green").click(function() {
    $("body").removeClass();
    $("body").addClass("green-background");
  });

  $("button#reset").click(function() {
    $("body").removeClass();
  });

  $("button#light").click(function() {
    $("body").removeClass();
  });

  $("button#dark").click(function() {
    $("body").removeClass();
    $("body").addClass("inverse-colors");
  });

});
