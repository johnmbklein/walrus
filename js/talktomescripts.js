$(function(){

  $("#hello").click(function(){
    $("#user-list").prepend("<li>Heeeey!</li>");
    $("#computer-list").prepend("<li>Hello</li>");
    $("#user-list").children("li").first().click(function() {
      $(this).remove();
    });
    $("#computer-list").children("li").first().click(function() {
      $(this).remove();
    });
  });

  $("#goodbye").click(function(){
    $("#user-list").prepend("<li>See ya!</li>");
    $("#computer-list").prepend("<li>Goodbye</li>");
    $("#user-list").children("li").first().click(function() {
      $(this).remove();
    });
    $("#computer-list").children("li").first().click(function() {
      $(this).remove();
    });
  });

  $("#stop").click(function(){
    $("#user-list").prepend("<li>This is creepy. Stop.</li>");
    $("#computer-list").prepend("<li>Sorry.</li>");
    $("#user-list").children("li").first().click(function() {
      $(this).remove();
    });
    $("#computer-list").children("li").first().click(function() {
      $(this).remove();
    });
  });




});
